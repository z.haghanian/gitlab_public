# Use the official Node.js image as the base image
FROM node:14
#set the working directory in the container
WORKDIR /app

# Copy the package.json and package-lock.json files
COPY package*.json ./

# Install Node.js dependencies
 RUN npm install


COPY . .


# Expose the port the app runs on
 EXPOSE 3000

# Define the command to run your application
 CMD [ "node", "app.js" ]
